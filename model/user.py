from flask_mysqldb import  MySQL
from db import  db
class User(db.Model):
    __tablename__='user'
    id=db.Column(db.Integer,db.Sequence('user_id',start=1, increment=1),primary_key=True)
    username=db.Column(db.String(20),nullable=False)
    password=db.Column(db.String(20),nullable=False)

    def __init__(self,  username, password):

        self.username = username
        self.password = password

    def __init__(self, id, username, password):
        self.id = id
        self.username = username
        self.password = password



    def __str__(self):
        return f"User(id={self.id}  name ={self.username}  password={self.password} )"
    @classmethod
    def find_by_username(cls,username):
        x=cls.query.filter_by(username=username).first()
        return x
    @classmethod
    def find_all(cls):
        return {"users":[user.json() for user in cls.query.all()]}



    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()
    def json(self):
        return {'id':self.id,'username':self.username,'password':self.password}

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return self
        except Exception as e:
            print(e)
            return None
    def update(self):
        user= self.query.filter_by(id=self.id).first()
        user.username=self.username
        user.password=self.password
        try:
            db.session.commit()
            return user
        except:
            return None
    def delete(self):
        db.session.delete(self)
        db.session.commit()

