from flask import Flask
import  redis
from rq import Queue
from service.mail_service import signupMail
from flask_restful import Api
from resources.Student import Student
from flask_mail import Mail
from flask_jwt import JWT
from service.security import identity, authenticate
from flask_mysqldb import MySQL
from resources.signup import Signup
from resources.login import Login
from resources.profile import Profile
app = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'

app.config['MYSQL_PASSWORD'] = 'Balashanthi#7'

app.config['MYSQL_DB'] = 'flaskdb'


app.debug = True
app.config['SECRET_KEY'] = 'secret'
jwt = JWT(app, authenticate, identity)

app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True
# app.config.from_pyfile('config.py')
app.config['MAIL_USERNAME'] = 'deepashdeepika77@gmail.com'
app.config['MAIL_PASSWORD'] = 'Balashanthi#77'

app.config['PROPAGATE_EXCEPTIONS']=True

app.config['SQLALCHEMy_TRACK_MODIFICATIONS']=False
app.config['SQLALCHEMY_DATABASE_URI']='mysql://root:Balashanthi#7@localhost/flaskdb'
mail = Mail(app)

mysql = MySQL(app)
api = Api(app)

r = redis.Redis()
q = Queue(connection=r)

#queue
@app.route("/queue")
def queu():
    task = (q.enqueue(signupMail, args=('deepashdeepika7@gmail.com',)))
    print("queue scheduled")

    return f" task id {task.id} enqueued at {task.enqueued_at} , {len(q)} is pending"



api.add_resource(Student, "/student", "/student/<int:id>")

api.add_resource(Signup, '/signup')

api.add_resource(Login,'/login')

api.add_resource(Profile,'/profile','/profile/<int:id>')

#table creation
@app.before_first_request
def create_table():
    db.create_all()

if __name__ == '__main__':
    from db import db
    db.init_app(app)
    app.run(port=5001, debug=True)
