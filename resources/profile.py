from service.mail_service import signupMail
from model.user import User
from db import db
from flask_jwt import jwt_required
from flask_restful import Resource
from flask import render_template, request, make_response
class Profile(Resource):
    def get(self,id=None):
        if id:
            user=User.find_by_id(id)
            if user:
                return user.json()
            else:
                return None

        else:
            return User.find_all()
    @jwt_required()
    def post(self,id):
        body = request.get_json()
        user=User(id,body['username'],body['password'])
        if user.update():
            return user.json()
        else:
            None