from flask_restful import Resource
from flask import render_template, request, make_response
import  redis
from rq import Queue
from service.mail_service import signupMail
from model.user import User
class Signup(Resource):
    r = redis.Redis()
    q = Queue(connection=r)
    def get(self):
        return make_response(render_template('signup.html'), 200)

    def post(self):
        form_data = request.form

        username = form_data['username']
        password = form_data['password']
        user=User(username=username,password=password)
        if user.save():
            signupMail(username)
            return user.json()
        else:
            return None