from flask_restful import Resource
from flask import render_template,make_response,request

from flask_mysqldb import  MySQL
class Login(Resource):
    def get(self):
        return make_response(render_template('login.html'),200)
