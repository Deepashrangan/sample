from flask_restful import Resource,reqparse
from flask import jsonify,request
from flask_jwt import JWT, jwt_required

from flask_mail import Mail,Message
class Student(Resource):
    a = [
        {
            "id": 1,
            "skill": [
                {
                    1: "java 8",
                },
                {
                    2: "python 3"
                }
            ]
        },
        {
            "id": 2,
            "skill": [
                {
                    1: "java 8",
                },
                {
                    2: "python 3"
                }
            ]

        }
    ]
    @jwt_required()
    def get(self, id=None):
        if id==None:
            return jsonify(self.a)

        for x in self.a:
            if x["id"] == id:
                message=Message("data sheet",sender='deepashdeepika77@gmail.com',recipients=['deepashdeepika7@gmail.com','monika.1602126@srec.ac.in'])
                message.body="heelo there "
                message.html="<h1>Im h1</h1> <p style='color:red'>whats ur problem </p>"
                Mail().send(message)
                return jsonify(x)

        return {"students": "No user found"}, 404

    def post(self):
        user = request.get_json()
        self.a.append(user)
        return jsonify(user)

    def delete(self,id):
        for x in self.a:
            if x.get("id") == id:
                self.a.remove(x)
                return x, 200
        return {"students": "No user found"}, 404

    def put(self,id):
        parser=reqparse.RequestParser()
        parser.add_argument('name',type=str,required=True,help="name cannnot be empty")
       # user = request.get_json()
        user=parser.parse_args()
        for x in self.a:
            if x.get("id") == id:
                x["name"] = user["name"]
                return x
        return {"students": "No user found"}, 404



